package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class AuditRepositoryTest {
	@Autowired
	private AuditRepository auditRepository;
	
	@Test
	@Order(1)
	public void testCreate() {
		Audit audit = new Audit();
		audit.setEventType(EventType.VIREMENT);
		audit.setMessage("test");
		audit = auditRepository.save(audit);
		
		assertNotNull(audit);
	}
	
	@Test
	@Order(2)
	public void testGetOne() {
		assertEquals("test", auditRepository.findAll().get(0).getMessage());
	}

	@Test
	@Order(3)
	public void testGetAll() {
		assertEquals(1, auditRepository.findAll().size());
		
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Audit audit = auditRepository.findByMessage("test");
		audit.setMessage("testx");
		
		auditRepository.save(audit);
		
		assertEquals("testx",auditRepository.findByMessage("testx").getMessage());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		Audit audit = auditRepository.findByMessage("testx");
		auditRepository.deleteById(audit.getId());
		assertThat(auditRepository.existsById(audit.getId())).isFalse();
	}
	
}
