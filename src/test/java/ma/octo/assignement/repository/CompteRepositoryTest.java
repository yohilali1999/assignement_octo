package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class CompteRepositoryTest {
	@Autowired
	private CompteRepository compteRepository;
	
	@Test
	@Order(1)
	public void testCreate() {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male"); 
		
		Compte compte = new Compte();
		compte.setNrCompte("num1");
		compte.setRib("rib1");
		compte.setSolde(BigDecimal.TEN);
		compte = compteRepository.save(compte);
		
		assertNotNull(compte);
	}
	
	@Test
	@Order(2)
	public void testGetOne() {
		assertEquals("num1", compteRepository.findAll().get(0).getNrCompte());
	}

	@Test
	@Order(3)
	public void testGetAll() {
		assertEquals(1, compteRepository.findAll().size());
		
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Compte compte = compteRepository.findByNrCompte("num1");
		compte.setNrCompte("numx");
		
		compteRepository.save(compte);
		
		assertEquals("numx",compteRepository.findByNrCompte("numx").getNrCompte());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		Compte compte = compteRepository.findByNrCompte("numx");
		compteRepository.deleteById(compte.getId());
		assertThat(compteRepository.existsById(compte.getId())).isFalse();
	}
	
}
