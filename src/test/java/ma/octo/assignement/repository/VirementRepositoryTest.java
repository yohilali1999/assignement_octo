package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Virement;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class VirementRepositoryTest {
	@Autowired
	private VirementRepository virementRepository; 
	@Test
	@Order(1)
	public void testCreate() {
		Virement virement = new Virement();
		virement.setMontantVirement(BigDecimal.TEN);
		virement.setMotifVirement("motif1");
		
		virement = virementRepository.save(virement);
		
		assertNotNull(virement);
	}
	
	@Test
	@Order(2)
	public void testGetOne() {
		assertEquals("motif1", virementRepository.findAll().get(0).getMotifVirement());
	}

	@Test
	@Order(3)
	public void testGetAll() {
		assertEquals(1, virementRepository.findAll().size());
		
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Virement virement = virementRepository.findByMotifVirement("motif1");
		virement.setMotifVirement("motifx");
		
		virementRepository.save(virement);
		
		assertEquals("motifx",virementRepository.findByMotifVirement("motifx").getMotifVirement());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		Virement virement = virementRepository.findByMotifVirement("motifx");
		virementRepository.deleteById(virement.getId());
		assertThat(virementRepository.existsById(virement.getId())).isFalse();
	}

}
