package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.Virement;

public interface VirementRepository extends JpaRepository<Virement, Long> {
	Virement findByMotifVirement(String motif);
}
