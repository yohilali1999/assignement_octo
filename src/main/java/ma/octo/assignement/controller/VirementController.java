package ma.octo.assignement.controller;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VirementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/virement")
class VirementController {
    @Autowired
    private VirementService virementService;

    @GetMapping
    List<Virement> getAllVirement() {
    	return virementService.getAllVirement();
    }

	@GetMapping("{id}")
	public Virement getVirement(@PathVariable("id") Long id) {
		return virementService.getVirement(id);
	}
	
	@PostMapping
	public Virement createVirement(@RequestBody Virement virement) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		return virementService.createVirement(virement);
	}
	
	@PutMapping("{id}")
	public Virement updateVersement(@RequestBody Virement virement,@PathVariable("id") Long id) {
		return virementService.updateVirement(virement, id);
	}
	
	@DeleteMapping("{id}")
	public Virement deleteVirement(@PathVariable("id") Long id) {
		return virementService.deleteVirement(id);
	}
    

}
