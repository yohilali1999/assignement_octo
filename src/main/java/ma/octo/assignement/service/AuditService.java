package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Audit;

public interface AuditService {
	List<Audit> getAllAudit();
	Audit getAudit(Long id);	
	Audit createAudit(Audit audit);
	Audit updateAudit(Audit audit, Long id);
	Audit deleteAudit(Long id);
}
