package ma.octo.assignement.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;

@Service
@Transactional
public class VersementServiceImpl implements VersementService{
	@Autowired
	private AuditService auditService;
	@Autowired
	private VersementRepository versementRepository;
	@Autowired
	private CompteService compteService;
	@Override
	public List<Versement> getAllVersement() {
		return versementRepository.findAll();
	}

	@Override
	public Versement getVersement(Long id) {
		return versementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Versement", "id", id));
	}

	@Override
	public Versement createVersement(Versement versement) {
		Compte compte = versement.getCompteBeneficiaire();
		compte.setSolde(compte.getSolde().add(versement.getMontantVirement()));
		this.compteService.updateCompte(compte, compte.getId());
		
		Audit audit = new Audit();
        audit.setMessage("Versement depuis " + versement.getNom_prenom_emetteur() + " vers " + versement.getCompteBeneficiaire().getNrCompte() +
        		" d\'un montant de " + versement.getMontantVirement().toString());
        audit.setEventType(EventType.VERSEMENT);
        auditService.createAudit(audit);
		
		return versementRepository.save(versement);
	}

	@Override
	public Versement updateVersement(Versement versement, Long id) {
		Versement oldVersement = versementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Versement", "id", id));
		oldVersement.setCompteBeneficiaire(versement.getCompteBeneficiaire());
		oldVersement.setDateExecution(versement.getDateExecution());
		oldVersement.setMontantVirement(versement.getMontantVirement());
		oldVersement.setMotifVersement(versement.getMotifVersement());
		oldVersement.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
		return oldVersement;
	}

	@Override
	public Versement deleteVersement(Long id) {
		Versement oldVersement = versementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Versement", "id", id));
		versementRepository.deleteById(id);
		return oldVersement;
	}

	
}
