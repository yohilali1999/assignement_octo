package ma.octo.assignement.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.UtilisateurService;

@Service
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurRepository; 
	
	@Override
	public List<Utilisateur> getAllUtilisateur() {
		return utilisateurRepository.findAll();
	}

	@Override
	public Utilisateur getUtilisateur(Long id) {
		return utilisateurRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", id));
	}

	@Override
	public Utilisateur createUtilisateur(Utilisateur utilisateur) {
		return utilisateurRepository.save(utilisateur);
	}

	@Override
	public Utilisateur updateUtilisateur(Utilisateur utilisateur, Long id) {
		Utilisateur oldUtilisateur = utilisateurRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", id));
		oldUtilisateur.setBirthdate(utilisateur.getBirthdate());
		oldUtilisateur.setFirstname(utilisateur.getFirstname());
		oldUtilisateur.setGender(utilisateur.getGender());
		oldUtilisateur.setLastname(utilisateur.getLastname());
		oldUtilisateur.setUsername(utilisateur.getUsername());
		return utilisateurRepository.save(oldUtilisateur);
	}

	@Override
	public Utilisateur deleteUtilisateur(Long id) {
		Utilisateur utilisateur = utilisateurRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", id));
		utilisateurRepository.deleteById(id);
		return utilisateur;
	}

}
